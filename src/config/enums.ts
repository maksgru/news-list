export enum EnvEnum {
  LOCAL = 'local',
  DEVELOPMENT = 'development',
  PRODUCTION = 'production'
}
