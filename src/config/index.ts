import { EnvEnum } from './enums';
import { type IConfig } from './interfaces';

const env = process.env.REACT_APP_ENV as EnvEnum;

const localConfig: IConfig = {
  env: EnvEnum.LOCAL,
  news: {
    baseUrl: '',
    apiKey: '',
    lang: 'en'
  }
};

const getConfig = (...configs: IConfig[]): IConfig => {
  const config = configs.find(el => el.env === env);

  return (config !== undefined) ? config : localConfig;
};

export const config = getConfig(localConfig);
