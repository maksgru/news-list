import { type EnvEnum } from './enums';

export interface INewsConfig {
  readonly baseUrl: string;
  readonly apiKey: string;
  readonly lang: string;
}

export interface IConfig {
  readonly env: EnvEnum;
  readonly news: INewsConfig;
}
