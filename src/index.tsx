import React from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { store } from './store';
import { App } from './app/app';
import reportWebVitals from './reportWebVitals';
import { CssBaseline } from '@mui/material';
import MaterialProvider from './theme/material-ui-provider';
import { BrowserRouter } from 'react-router-dom';

const container = document.getElementById('root') as HTMLElement;
const root = createRoot(container);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <MaterialProvider>
        <BrowserRouter>
          <CssBaseline />
          <App />
        </BrowserRouter>
      </MaterialProvider>
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
