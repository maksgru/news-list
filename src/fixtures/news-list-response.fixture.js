export const newsListFixture = {
  status: 'ok',
  news: [
    {
      id: '4e0b3b56-cd57-4102-b367-237b6814cf85',
      title: "Here's why Jim Cramer is sticking with Foot Locker despite terrible quarter and big stock drop",
      description: 'When starting a position in Foot Locker in March, we knew the company would not be fixed quickly. But we still believe it can be fixed.',
      url: 'https://www.cnbc.com/2023/05/19/heres-why-jim-cramer-is-sticking-with-foot-locker-after-terrible-quarter-and-big-stock-drop.html',
      author: 'Zev Fima',
      image: 'https://image.cnbcfm.com/api/v1/image/105231736-GettyImages-591788116.jpg?v=1684516483&w=1920&h=1080',
      language: 'en',
      category: [
        'general'
      ],
      published: '2023-05-19 17:25:57 +0000'
    },
    {
      id: 'd06ebcc4-bc23-48b8-973f-9d5e6267c3df',
      title: "Our conviction in Morgan Stanley remains solid, despite the CEO's planned departure",
      description: "We're disappointed to see James Gorman go but have full confidence in the bank going forward — and see a potential buying opportunity.",
      url: 'https://www.cnbc.com/2023/05/19/our-conviction-in-morgan-stanley-remains-solid.html',
      author: 'Kevin Stankiewicz',
      image: 'https://image.cnbcfm.com/api/v1/image/106745568-1602760851038-gettyimages-1146927874-CHINA_JAMES_GORMAN.jpeg?v=1684517032&w=1920&h=1080',
      language: 'en',
      category: [
        'general'
      ],
      published: '2023-05-19 17:25:05 +0000'
    },
    {
      id: 'f0f2674f-4758-4ea7-b7f2-793afe293485',
      title: 'LIV Golf’s DeChambeau seizes lead at PGA Championship',
      description: 'ROCHESTER, New York: Bryson DeChambeau delivered a credibility boost to LIV Golf, firing a 4-under par 66 to seize a one-stroke lead in Thursday’s opening round of the PGA Championship. The slimmed-down 2020 US Open champion, coming off 2022 left wrist surgery, struck a competitor with an errant sho...',
      url: 'https://www.arabnews.com/node/2306276/golf',
      author: '@Arab_News',
      image: 'https://www.arabnews.com/sites/default/files/styles/660x371_watermarksaudi/public/main-image/2023/05/19/3824671-833637728.jpg?itok=CSBOuoGK',
      language: 'en',
      category: [
        'sports'
      ],
      published: '2023-05-19 17:24:02 +0000'
    },
    {
      id: 'd9c083a2-2be1-41c1-82c3-4bfd07a42d52',
      title: 'Murray’s big fourth quarter propels Nuggets past Lakers 108-103 for 2-0 lead in West finals',
      description: 'DENVER: Jamal Murray scored 23 of his 37 points in the fourth quarter, propelling the Denver Nuggets to a 108-103 come-from-behind win over the Los Angeles Lakers on Thursday night for a 2-0 lead in the Western Conference finals. Nikola Jokic had his 13th playoff triple-double with 23 points, 17 reb...',
      url: 'https://www.arabnews.com/node/2306316/sport',
      author: 'AP',
      image: 'https://www.arabnews.com/sites/default/files/styles/660x371_watermarksaudi/public/main-image/2023/05/19/3824791-624050401.jpg?itok=cree6kO5',
      language: 'en',
      category: [
        'sports'
      ],
      published: '2023-05-19 17:24:00 +0000'
    },
    {
      id: 'a525e006-22e3-4532-af80-1a8aef5cd10a',
      title: 'Lexi Thompson and Lydia Ko headline the Aramco Team Series Florida',
      description: 'FLORIDA: Two of the best players in the world of women’s golf are anticipating a week of strong competition and challenging conditions ahead of the Aramco Team Series presented by PIF at Trump International Golf Club, West Palm Beach, from May 19-21. Home favorite Lexi Thompson is looking forward to...',
      url: 'https://www.arabnews.com/node/2306411/sport',
      author: '@Arab_News',
      image: 'https://www.arabnews.com/sites/default/files/styles/660x371_watermarksaudi/public/main-image/2023/05/19/3825131-394584123.jpg?itok=ID44xwoK',
      language: 'en',
      category: [
        'sports'
      ],
      published: '2023-05-19 17:23:58 +0000'
    },
    {
      id: '5400e299-9926-414a-add1-7d7f38ab79d5',
      title: 'MIBR and Fnatic claim ‘Counter-Strike: Global Offensive’ glory, as Gamers Without Borders reaches dramatic finale',
      description: 'RIYADH: MIBR have triumphed in the North American section while Fnatic claimed victory in the European section of the men’s Counter-Strike: Global Offensive (CS:GO) tournament at Gamers Without Borders, the world’s largest charitable esports event. Held virtually by the Saudi Esports Federation unde...',
      url: 'https://www.arabnews.com/node/2306421/e-sport',
      author: '@Arab_News',
      image: 'https://www.arabnews.com/sites/default/files/styles/660x371_watermarksaudi/public/main-image/2023/05/19/3825146-1789920415.jpg?itok=y8RE9BWp',
      language: 'en',
      category: [
        'sports'
      ],
      published: '2023-05-19 17:23:57 +0000'
    },
    {
      id: '10da19ec-7033-4f08-a9d3-411daab62763',
      title: 'Al-Rajhi leads Tabuk-NEOM rally after qualifiers',
      description: 'TABUK: Yazeed Al-Rajhi, Sultan Al-Balooshi and Haitham Al-Tuwaijri set the quickest times in the car, motorcycle and quad categories in front of huge crowds on the Qualifying Stage, held on the outskirts of Tabuk, that kickstarted the weekend’s action in the first Tabuk-Neom Rally on Thursday aftern...',
      url: 'https://www.arabnews.com/node/2306426/motorsport',
      author: '@Arab_News',
      image: 'https://www.arabnews.com/sites/default/files/styles/660x371_watermarksaudi/public/main-image/2023/05/19/3825156-465018990.jpeg?itok=raWvSS81',
      language: 'en',
      category: [
        'sports'
      ],
      published: '2023-05-19 17:23:55 +0000'
    },
    {
      id: 'a29af7a2-a8ae-404a-8b54-1a5fe07985e3',
      title: 'Seth Rollins to compete with AJ Styles for WWE World Heavyweight Championship in Jeddah',
      description: 'RIYADH: The stage is set for a historic evening at WWE Night of Champions, as AJ Styles will face Seth Rollins for the chance to become the first world heavyweight champion in the headline match at Jeddah Superdome on Saturday, May 27. Battling his way into the championship match, Rollins had to go ...',
      url: 'https://www.arabnews.com/node/2306476/sport',
      author: '@Arab_News',
      image: 'https://www.arabnews.com/sites/default/files/styles/660x371_watermarksaudi/public/main-image/2023/05/19/3825266-1806101520.jpg?itok=TWMKUwGr',
      language: 'en',
      category: [
        'sports'
      ],
      published: '2023-05-19 17:23:54 +0000'
    },
    {
      id: '6d2dd995-fee2-4ebc-b9c5-0e01d132b6cd',
      title: 'Diversity of Nationalities, Professions Among Migrants at US Mexico Border',
      description: 'Majority of migrants now from Venezuela, Cuba, Nicaragua, and Haiti, and as far away as China and Afghanistan',
      url: 'https://www.voanews.com/a/diversity-of-nationalities-professions-among-migrants-at-us-mexico-border-/7100770.html',
      author: 'Aline Barros',
      image: 'https://gdb.voanews.com/01000000-0aff-0242-5dc8-08db58879244_tv_w1200_r1.jpg',
      language: 'en',
      category: [
        'world'
      ],
      published: '2023-05-19 17:23:34 +0000'
    },
    {
      id: 'f3bfb8e9-fe58-4aa3-8974-9100243a449f',
      title: 'ONE-PEACE: Exploring One General Representation Model Toward Unlimited Modalities. (arXiv:2305.11172v1 [cs.CV])',
      description: 'In this work, we explore a scalable way for building a general representation\nmodel toward unlimited modalities. We release ONE-PEACE, a highly extensible\nmodel with 4B parameters that can seamlessly align and integrate\nrepresentations across vision, audio, and language modalities. The architecture\n...',
      url: 'http://arxiv.org/abs/2305.11172',
      author: '<a href="http://arxiv.org/find/cs/1/au:+Wang_P/0/1/0/all/0/1">Peng Wang</a>, <a href="http://arxiv.org/find/cs/1/au:+Wang_S/0/1/0/all/0/1">Shijie Wang</a>, <a href="http://arxiv.org/find/cs/1/au:+Lin_J/0/1/0/all/0/1">Junyang Lin</a>, <a href="http://arxiv.org/find/cs/1/au:+Bai_S/0/1/0/all/0/1">Shuai',
      image: 'None',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '4a8da07f-f9de-4c67-8b53-1b501c363a8f',
      title: 'Unsupervised Multi-channel Separation and Adaptation. (arXiv:2305.11151v1 [cs.SD])',
      description: 'A key challenge in machine learning is to generalize from training data to an\napplication domain of interest. This work generalizes the recently-proposed\nmixture invariant training (MixIT) algorithm to perform unsupervised learning\nin the multi-channel setting. We use MixIT to train a model on far-f...',
      url: 'http://arxiv.org/abs/2305.11151',
      author: '<a href="http://arxiv.org/find/cs/1/au:+Han_C/0/1/0/all/0/1">Cong Han</a>, <a href="http://arxiv.org/find/cs/1/au:+Wilson_K/0/1/0/all/0/1">Kevin Wilson</a>, <a href="http://arxiv.org/find/cs/1/au:+Wisdom_S/0/1/0/all/0/1">Scott Wisdom</a>, <a href="http://arxiv.org/find/cs/1/au:+Hershey_J/0/1/0/all/0',
      image: 'None',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '9e0d5bde-a805-492f-9d55-9e26a6054049',
      title: 'mdctGAN: Taming transformer-based GAN for speech super-resolution with Modified DCT spectra. (arXiv:2305.11104v1 [eess.AS])',
      description: 'Speech super-resolution (SSR) aims to recover a high resolution (HR) speech\nfrom its corresponding low resolution (LR) counterpart. Recent SSR methods\nfocus more on the reconstruction of the magnitude spectrogram, ignoring the\nimportance of phase reconstruction, thereby limiting the recovery quality...',
      url: 'http://arxiv.org/abs/2305.11104',
      author: '<a href="http://arxiv.org/find/eess/1/au:+Shuai_C/0/1/0/all/0/1">Chenhao Shuai</a>, <a href="http://arxiv.org/find/eess/1/au:+Shi_C/0/1/0/all/0/1">Chaohua Shi</a>, <a href="http://arxiv.org/find/eess/1/au:+Gan_L/0/1/0/all/0/1">Lu Gan</a>, <a href="http://arxiv.org/find/eess/1/au:+Liu_H/0/1/0/all/0/1',
      image: 'None',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '0a34b341-c34b-4311-ba2b-74fe3e45b191',
      title: 'A Comparative Study on E-Branchformer vs Conformer in Speech Recognition, Translation, and Understanding Tasks',
      description: 'Conformer, a convolution-augmented Transformer variant, has become the de\nfacto encoder architecture for speech processing due to its superior\nperformance in various tasks, including automatic speech recognition (ASR),\nspeech translation (ST) and spoken language understanding (SLU). Recently, a\nnew ...',
      url: 'https://arxiv.org/abs/2305.11073',
      author: 'Watanabe, Shinji',
      image: 'https://static.arxiv.org/icons/twitter/arxiv-logo-twitter-square.png',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '88ba21dd-2b9e-45bf-b7bd-801be0045e96',
      title: 'Self-supervised Fine-tuning for Improved Content Representations by Speaker-invariant Clustering',
      description: 'Self-supervised speech representation models have succeeded in various tasks,\nbut improving them for content-related problems using unlabeled data is\nchallenging. We propose speaker-invariant clustering (Spin), a novel\nself-supervised learning method that clusters speech representations and\nperforms...',
      url: 'https://arxiv.org/abs/2305.11072',
      author: 'Glass, James',
      image: 'https://static.arxiv.org/icons/twitter/arxiv-logo-twitter-square.png',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: 'eafe1eb9-77df-4f74-a63e-89aa1e2e0919',
      title: 'FunASR: A Fundamental End-to-End Speech Recognition Toolkit. (arXiv:2305.11013v1 [cs.SD])',
      description: "This paper introduces FunASR, an open-source speech recognition toolkit\ndesigned to bridge the gap between academic research and industrial\napplications. FunASR offers models trained on large-scale industrial corpora\nand the ability to deploy them in applications. The toolkit's flagship model,\nParaf...",
      url: 'http://arxiv.org/abs/2305.11013',
      author: '<a href="http://arxiv.org/find/cs/1/au:+Gao_Z/0/1/0/all/0/1">Zhifu Gao</a>, <a href="http://arxiv.org/find/cs/1/au:+Li_Z/0/1/0/all/0/1">Zerui Li</a>, <a href="http://arxiv.org/find/cs/1/au:+Wang_J/0/1/0/all/0/1">Jiaming Wang</a>, <a href="http://arxiv.org/find/cs/1/au:+Luo_H/0/1/0/all/0/1">Haoneng L',
      image: 'None',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: 'cf1db6bf-ce1c-4cbb-ba1c-9e49754b62c0',
      title: 'Making More of Little Data: Improving Low-Resource Automatic Speech Recognition Using Data Augmentation. (arXiv:2305.10951v1 [cs.CL])',
      description: 'The performance of automatic speech recognition (ASR) systems has advanced\nsubstantially in recent years, particularly for languages for which a large\namount of transcribed speech is available. Unfortunately, for low-resource\nlanguages, such as minority languages, regional languages or dialects, ASR...',
      url: 'http://arxiv.org/abs/2305.10951',
      author: '<a href="http://arxiv.org/find/cs/1/au:+Bartelds_M/0/1/0/all/0/1">Martijn Bartelds</a>, <a href="http://arxiv.org/find/cs/1/au:+San_N/0/1/0/all/0/1">Nay San</a>, <a href="http://arxiv.org/find/cs/1/au:+McDonnell_B/0/1/0/all/0/1">Bradley McDonnell</a>, <a href="http://arxiv.org/find/cs/1/au:+Jurafsky',
      image: 'None',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '5912cc42-639d-42fc-a55c-b7c01873a9e6',
      title: 'Improving Generalization Ability of Countermeasures for New Mismatch Scenario by Combining Multiple Advanced Regularization Terms',
      description: 'The ability of countermeasure models to generalize from seen speech synthesis\nmethods to unseen ones has been investigated in the ASVspoof challenge.\nHowever, a new mismatch scenario in which fake audio may be generated from real\naudio with unseen genres has not been studied thoroughly. To this end,...',
      url: 'https://arxiv.org/abs/2305.10940',
      author: 'Yamagishi, Junichi',
      image: 'https://static.arxiv.org/icons/twitter/arxiv-logo-twitter-square.png',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '1daab19c-829e-491e-bf1d-218e32ce81d1',
      title: 'Diffusion-Based Mel-Spectrogram Enhancement for Personalized Speech Synthesis with Found Data',
      description: 'Creating synthetic voices with found data is challenging, as real-world\nrecordings often contain various types of audio degradation. One way to address\nthis problem is to pre-enhance the speech with an enhancement model and then\nuse the enhanced data for text-to-speech (TTS) model training. Ideally,...',
      url: 'https://arxiv.org/abs/2305.10891',
      author: 'Lee, Tan',
      image: 'https://static.arxiv.org/icons/twitter/arxiv-logo-twitter-square.png',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '659008c2-92ca-4344-ae5a-41ef9bbe9389',
      title: 'GETMusic: Generating Any Music Tracks with a Unified Representation and Diffusion Framework. (arXiv:2305.10841v1 [cs.SD])',
      description: 'Symbolic music generation aims to create musical notes, which can help users\ncompose music, such as generating target instrumental tracks from scratch, or\nbased on user-provided source tracks. Considering the diverse and flexible\ncombination between source and target tracks, a unified model capable ...',
      url: 'http://arxiv.org/abs/2305.10841',
      author: '<a href="http://arxiv.org/find/cs/1/au:+Lv_A/0/1/0/all/0/1">Ang Lv</a>, <a href="http://arxiv.org/find/cs/1/au:+Tan_X/0/1/0/all/0/1">Xu Tan</a>, <a href="http://arxiv.org/find/cs/1/au:+Lu_P/0/1/0/all/0/1">Peiling Lu</a>, <a href="http://arxiv.org/find/cs/1/au:+Ye_W/0/1/0/all/0/1">Wei Ye</a>, <a href',
      image: 'None',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '70d6e086-176d-4d09-8f31-98ae49ddf868',
      title: 'A Lexical-aware Non-autoregressive Transformer-based ASR Model. (arXiv:2305.10839v1 [cs.CL])',
      description: 'Non-autoregressive automatic speech recognition (ASR) has become a mainstream\nof ASR modeling because of its fast decoding speed and satisfactory result. To\nfurther boost the performance, relaxing the conditional independence assumption\nand cascading large-scaled pre-trained models are two active re...',
      url: 'http://arxiv.org/abs/2305.10839',
      author: '<a href="http://arxiv.org/find/cs/1/au:+Lin_C/0/1/0/all/0/1">Chong-En Lin</a>, <a href="http://arxiv.org/find/cs/1/au:+Chen_K/0/1/0/all/0/1">Kuan-Yu Chen</a>',
      image: 'None',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: 'ca403560-74de-4318-87cb-5293ad096e13',
      title: 'FastFit: Towards Real-Time Iterative Neural Vocoder by Replacing U-Net Encoder With Multiple STFTs',
      description: 'This paper presents FastFit, a novel neural vocoder architecture that\nreplaces the U-Net encoder with multiple short-time Fourier transforms (STFTs)\nto achieve faster generation rates without sacrificing sample quality. We\nreplaced each encoder block with an STFT, with parameters equal to the tempor...',
      url: 'https://arxiv.org/abs/2305.10823',
      author: 'Park, Heayoung',
      image: 'https://static.arxiv.org/icons/twitter/arxiv-logo-twitter-square.png',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: 'bb8532b3-ce69-47b9-98c4-5557e1f801fb',
      title: 'Locate and Beamform: Two-dimensional Locating All-neural Beamformer for Multi-channel Speech Separation',
      description: "Recently, stunning improvements on multi-channel speech separation have been\nachieved by neural beamformers when direction information is available.\nHowever, most of them neglect to utilize speaker's 2-dimensional (2D) location\ncues contained in mixture signal, which limits the performance when two ...",
      url: 'https://arxiv.org/abs/2305.10821',
      author: 'Wang, Fei',
      image: 'https://static.arxiv.org/icons/twitter/arxiv-logo-twitter-square.png',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: 'aab92062-1e95-4624-9605-c076f429da21',
      title: 'TempAdaCos: Learning Temporally Structured Embeddings for Few-Shot Keyword Spotting with Dynamic Time Warping',
      description: 'Few-shot keyword spotting (KWS) systems often utilize a sliding window of\nfixed size. Because of the varying lengths of different keywords or their\nspoken instances, choosing the right window size is a problem: A window should\nbe long enough to contain all necessary information needed to recognize a...',
      url: 'https://arxiv.org/abs/2305.10816',
      author: 'Cornaggia-Urrigshardt, Alessia',
      image: 'https://static.arxiv.org/icons/twitter/arxiv-logo-twitter-square.png',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '07c92d77-7919-4146-a1da-25b9adfc300e',
      title: 'Validation of an ECAPA-TDNN system for Forensic Automatic Speaker Recognition under case work conditions',
      description: 'Different variants of a Forensic Automatic Speaker Recognition (FASR) system\nbased on Emphasized Channel Attention, Propagation and Aggregation in Time\nDelay Neural Network (ECAPA-TDNN) are tested under conditions reflecting those\nof a real forensic voice comparison case, according to the forensic_e...',
      url: 'https://arxiv.org/abs/2305.10805',
      author: 'Grimaldi, Mirko',
      image: 'https://static.arxiv.org/icons/twitter/arxiv-logo-twitter-square.png',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '1da46226-4898-4e34-b9ba-3c052f41e50a',
      title: 'Listen, Think, and Understand. (arXiv:2305.10790v1 [eess.AS])',
      description: 'The ability of artificial intelligence (AI) systems to perceive and\ncomprehend audio signals is crucial for many applications. Although significant\nprogress has been made in this area since the development of AudioSet, most\nexisting models are designed to map audio inputs to pre-defined, discrete so...',
      url: 'http://arxiv.org/abs/2305.10790',
      author: '<a href="http://arxiv.org/find/eess/1/au:+Gong_Y/0/1/0/all/0/1">Yuan Gong</a>, <a href="http://arxiv.org/find/eess/1/au:+Luo_H/0/1/0/all/0/1">Hongyin Luo</a>, <a href="http://arxiv.org/find/eess/1/au:+Liu_A/0/1/0/all/0/1">Alexander H. Liu</a>, <a href="http://arxiv.org/find/eess/1/au:+Karlinsky_L/0/',
      image: 'None',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '20952c26-3a1b-4f43-aa96-19743f8a1d05',
      title: 'Whisper-KDQ: A Lightweight Whisper via Guided Knowledge Distillation and Quantization for Efficient ASR. (arXiv:2305.10788v1 [cs.SD])',
      description: 'Due to the rapid development of computing hardware resources and the dramatic\ngrowth of data, pre-trained models in speech recognition, such as Whisper, have\nsignificantly improved the performance of speech recognition tasks. However,\nthese models usually have a high computational overhead, making i...',
      url: 'http://arxiv.org/abs/2305.10788',
      author: '<a href="http://arxiv.org/find/cs/1/au:+Shao_H/0/1/0/all/0/1">Hang Shao</a>, <a href="http://arxiv.org/find/cs/1/au:+Wang_W/0/1/0/all/0/1">Wei Wang</a>, <a href="http://arxiv.org/find/cs/1/au:+Liu_B/0/1/0/all/0/1">Bei Liu</a>, <a href="http://arxiv.org/find/cs/1/au:+Gong_X/0/1/0/all/0/1">Xun Gong</a',
      image: 'None',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '9b8a974b-c34b-43d1-b583-6133e3784e4d',
      title: 'Enhancing Speech Articulation Analysis using a Geometric Transformation of the X-ray Microbeam Dataset',
      description: 'Accurate analysis of speech articulation is crucial for speech analysis.\nHowever, X-Y coordinates of articulators strongly depend on the anatomy of the\nspeakers and the variability of pellet placements, and existing methods for\nmapping anatomical landmarks in the X-ray Microbeam Dataset (XRMB) fail ...',
      url: 'https://arxiv.org/abs/2305.10775',
      author: 'Espy-Wilson, Carol Y.',
      image: 'https://static.arxiv.org/icons/twitter/arxiv-logo-twitter-square.png',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '3f4f2543-0225-4f2e-b8b4-8e4d7b0bad3f',
      title: 'CLAPSpeech: Learning Prosody from Text Context with Contrastive Language-Audio Pre-training. (arXiv:2305.10763v1 [cs.SD])',
      description: 'Improving text representation has attracted much attention to achieve\nexpressive text-to-speech (TTS). However, existing works only implicitly learn\nthe prosody with masked token reconstruction tasks, which leads to low training\nefficiency and difficulty in prosody modeling. We propose CLAPSpeech, a...',
      url: 'http://arxiv.org/abs/2305.10763',
      author: '<a href="http://arxiv.org/find/cs/1/au:+Ye_Z/0/1/0/all/0/1">Zhenhui Ye</a>, <a href="http://arxiv.org/find/cs/1/au:+Huang_R/0/1/0/all/0/1">Rongjie Huang</a>, <a href="http://arxiv.org/find/cs/1/au:+Ren_Y/0/1/0/all/0/1">Yi Ren</a>, <a href="http://arxiv.org/find/cs/1/au:+Jiang_Z/0/1/0/all/0/1">Ziyue ',
      image: 'None',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '9bfe3bf9-53dc-4dd5-ac6b-6d9549f9bd1b',
      title: 'Noise-aware Speech Separation with Contrastive Learning. (arXiv:2305.10761v1 [cs.SD])',
      description: 'Recently, speech separation (SS) task has achieved remarkable progress driven\nby deep learning technique. However, it is still challenging to separate target\nsignals from noisy mixture, as neural model is vulnerable to assign background\nnoise to each speaker. In this paper, we propose a noise-aware ...',
      url: 'http://arxiv.org/abs/2305.10761',
      author: '<a href="http://arxiv.org/find/cs/1/au:+Zhang_Z/0/1/0/all/0/1">Zizheng Zhang</a>, <a href="http://arxiv.org/find/cs/1/au:+Chen_C/0/1/0/all/0/1">Chen Chen</a>, <a href="http://arxiv.org/find/cs/1/au:+Liu_X/0/1/0/all/0/1">Xiang Liu</a>, <a href="http://arxiv.org/find/cs/1/au:+Hu_Y/0/1/0/all/0/1">Yuche',
      image: 'None',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    },
    {
      id: '4d3de050-740a-4faf-8664-15c3ec391524',
      title: 'Diffusion-Based Speech Enhancement with Joint Generative and Predictive Decoders',
      description: 'Diffusion-based speech enhancement (SE) has been investigated recently, but\nits decoding is very time-consuming. One solution is to initialize the decoding\nprocess with the enhanced feature estimated by a predictive SE system. However,\nthis two-stage method ignores the complementarity between predic...',
      url: 'https://arxiv.org/abs/2305.10734',
      author: 'Mitsufuji, Yuki',
      image: 'https://static.arxiv.org/icons/twitter/arxiv-logo-twitter-square.png',
      language: 'en',
      category: [
        'academic',
        'EE',
        'AS'
      ],
      published: '2023-05-19 17:21:55 +0000'
    }
  ],
  page: 1
};
