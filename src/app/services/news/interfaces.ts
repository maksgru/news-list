import { type INewsItem } from 'src/app/interfaces/news-item.interface';

export interface INewsService {
  readonly fetchNews: () => Promise<INewsItem[]>;
  readonly fetchNewsItemById: (id: string) => Promise<INewsItem>;
}
