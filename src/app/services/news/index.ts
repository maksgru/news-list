import { config } from 'src/config';
import { NewsServiceMock } from './news-service.mock';
import { NewsService } from './news.service';
import { type INewsService } from './interfaces';
import { EnvEnum } from 'src/config/enums';

const getService = (): INewsService => {
  if (config.env === EnvEnum.LOCAL) {
    return new NewsServiceMock();
  }
  return new NewsService();
};

export const newsService = getService();
