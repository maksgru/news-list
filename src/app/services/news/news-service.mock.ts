import { type INewsItem } from 'src/app/interfaces/news-item.interface';
import { newsListFixture } from '../../../fixtures/news-list-response.fixture';
import { type INewsService } from './interfaces';

export class NewsServiceMock implements INewsService {
  public fetchNews = async (): Promise<INewsItem[]> => await Promise.resolve(newsListFixture.news);
  public fetchNewsItemById = async (id: string): Promise<INewsItem> => {
    const news = await Promise.resolve(newsListFixture.news.find(n => n.id === id));
    if (news === undefined) {
      throw new Error('News not found');
    }

    return news;
  };
}
