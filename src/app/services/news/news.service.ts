import { type INewsService } from './interfaces';
import { newsApi } from './news-api';
import { type INewsItem } from 'src/app/interfaces/news-item.interface';

export class NewsService implements INewsService {
  public fetchNews = async (): Promise<INewsItem[]> => await newsApi.get('');
  public fetchNewsItemById = async (id: string): Promise<INewsItem> => await newsApi.get('');
}
