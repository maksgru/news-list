import axios from 'axios';
import { config } from 'src/config';

export const newsApi = axios.create({
  baseURL: config.news.baseUrl,
  withCredentials: true
});

newsApi.interceptors.request.use((request) => {
  return request;
});

newsApi.interceptors.response.use(
  ({ data }) => data,
  async (err) => {
    return await Promise.reject(err.response);
  }
);
