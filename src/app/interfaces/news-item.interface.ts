export interface INewsItem {
  readonly id: string;
  readonly title: string;
  readonly description: string;
  readonly url: string;
  readonly author: string;
  readonly image: string;
  readonly language: string;
  readonly category: string[];
  readonly published: string;
}
