export interface IProfile {
  readonly id: string;
  readonly name: string;
}
