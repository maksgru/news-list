import React from 'react';
import { Container } from '@mui/material';
import { NewsList } from 'src/app/modules/news-list/news-list.module';

export const MainPage: React.FC = () => {
  return (
    <Container>
      <NewsList />
    </Container>
  );
};
