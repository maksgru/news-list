import React, { useEffect } from 'react';
import { Paper } from '@mui/material';
import { Avatar } from '../../components/avatar';
import { getNewsItemById, selectChosenNews } from '../../../store/slices/news.slice';
import { getAuthorName } from 'src/app/modules/news-list/news-card/helpers/get-author-name';
import { useParams } from 'react-router-dom';
import { useStoreDispatch } from 'src/store/hooks';
import { type IId } from 'src/app/interfaces/id.interface';

export const NewsPage: React.FC = () => {
  const newsItem = selectChosenNews();
  const params = useParams<Partial<IId>>();
  const dispatch = useStoreDispatch();

  useEffect(() => {
    if (params.id != null) {
      void dispatch(getNewsItemById(params.id));
    }
  }, []);

  if (newsItem === null) {
    return null;
  }

  const author = getAuthorName(newsItem.author);

  return (
    <Paper>
      <Avatar name={author} />
    </Paper>
  );
};
