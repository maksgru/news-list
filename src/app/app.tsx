import React from 'react';
import { Container } from '@mui/material';
import { Footer } from './modules/footer/footer.module';
import { Header } from './modules/header/header.module';
import { AppRouter } from '../router/app-router';

export const App: React.FC = () => {
  return (
    <Container sx={{ pt: 6 }}>
      <Header />
      <AppRouter />
      <Footer />
    </Container>
  );
};
