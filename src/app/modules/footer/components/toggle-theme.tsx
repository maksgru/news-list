import React from 'react';
import { Stack, type StackProps } from '@mui/material';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import LightModeIcon from '@mui/icons-material/LightMode';
import { useStoreDispatch, useStoreSelector } from 'src/store/hooks';
import { ThemeTypeEnum } from 'src/theme/enums';
import { selectTheme, toggleTheme } from 'src/store/slices/app.slice';
import { SwitchIos } from 'src/app/components/switch-ios';

export const ToggleTheme: React.FC<StackProps> = props => {
  const theme = useStoreSelector(selectTheme);
  const dispatch = useStoreDispatch();
  const handleChange = (): void => {
    const theme: ThemeTypeEnum = isChecked ? ThemeTypeEnum.LIGHT : ThemeTypeEnum.DARK;
    dispatch(toggleTheme(theme));
  };

  const isChecked = theme === ThemeTypeEnum.DARK;
  return (
    <Stack
      direction="row"
      alignItems="center"
      height={1}
      {...props}
    >
      <SwitchIos
        icon={<LightModeIcon color="primary" />}
        checkedIcon={<DarkModeIcon />}
        checked={isChecked}
        onChange={handleChange}
      />
    </Stack>
  );
};
