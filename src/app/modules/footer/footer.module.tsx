import React from 'react';
import { Box } from '@mui/material';
import { ToggleTheme } from './components/toggle-theme';

export const Footer: React.FC = () => {
  return (
    <Box
      position="fixed"
      width="100%"
      height="40px"
      bgcolor="primary.contrastText"
      bottom={0}
      left={0}
    >
      <ToggleTheme position="absolute" right={8} />
    </Box>);
};
