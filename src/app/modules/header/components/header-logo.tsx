import React from 'react';
import { Box } from '@mui/material';

export const HeaderLogo: React.FC = () => {
  return (
    <Box>
      <img
        src="https://upload.wikimedia.org/wikipedia/commons/archive/3/39/20080218213148%21Wikinews-breaking-news.svg"
        height="90%"
        width="300px"
        style={{ objectFit: 'cover' }}
      />
    </Box>
  );
};
