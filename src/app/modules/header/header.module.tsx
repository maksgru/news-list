import React from 'react';
import { Box } from '@mui/material';
import { HeaderLogo } from './components/header-logo';

export const Header: React.FC = () => {
  return (
    <Box
      position="fixed"
      width="100%"
      height="40px"
      bgcolor="primary.contrastText"
      top={0}
      left={0}
      zIndex={1000}
    >
      <HeaderLogo />
    </Box>);
};
