import React from 'react';
import { Card, CardActions, CardContent, CardHeader, CardMedia, Collapse, IconButton, Typography } from '@mui/material';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import DeleteIcon from '@mui/icons-material/Delete';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { getAuthorName } from './helpers/get-author-name';
import { type INewsItem } from 'src/app/interfaces/news-item.interface';
import { Avatar } from 'src/app/components/avatar';
import { ExpandMore } from 'src/app/components/expand-more';
import { getPublishDate } from './helpers/get-publish-date';
import { DEFAULT_IMAGE_URL } from './constants';
import { Link } from 'react-router-dom';

interface INewsCardProps {
  readonly newsItem: INewsItem;
}

export const NewsCard: React.FC<INewsCardProps> = (props) => {
  const { newsItem } = props;
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = (): void => {
    setExpanded(!expanded);
  };
  const author = getAuthorName(newsItem.author);
  const publishDate = getPublishDate(newsItem.published);

  return (
    <Card sx={{ maxWidth: 550, my: 2, mx: 'auto' }}>
      <CardHeader
        avatar={
          <Avatar name={author} />
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={author}
        subheader={publishDate}
      />
      <Link to={`news/${newsItem.id}`}>
        <CardMedia
          component='img'
          image={newsItem.image !== 'None' ? newsItem.image : DEFAULT_IMAGE_URL}
          alt='Image is not available'
          height="194"
        />
      </Link>
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {newsItem.title}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton>
          <FavoriteIcon />
        </IconButton>
        <IconButton>
          <DeleteIcon />
        </IconButton>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography>
            {newsItem.description}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
};
