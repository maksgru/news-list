import { isStringHtml } from 'src/utils/is-string-html';

export const getAuthorName = (author: string): string => {
  const isAuthorHtml = isStringHtml(author);
  if (isAuthorHtml) {
    return 'No Author';
  }
  if (author.split(' ').length >= 2) {
    return author;
  }
  if (author.split('_').length >= 2) {
    return author.replace('_', ' ');
  }

  return 'No Author';
};
