import { DateTime } from 'luxon';

export const getPublishDate = (stringDate: string): string => {
  const date = DateTime.fromSQL(stringDate);
  const month = date.monthLong as string;
  const day = date.day;
  const year = date.year;

  return `${month} ${day}, ${year}`;
};
