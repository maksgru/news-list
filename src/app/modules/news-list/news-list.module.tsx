import React, { useEffect } from 'react';
import { Box } from '@mui/material';
import { NewsCard } from 'src/app/modules/news-list/news-card/news-card';
import { useStoreDispatch, useStoreSelector } from 'src/store/hooks';
import { getNews } from 'src/store/slices/news.slice';

export const NewsList: React.FC = () => {
  const news = useStoreSelector(state => state.news.newsList);
  const dispatch = useStoreDispatch();

  useEffect(() => {
    void dispatch(getNews());
  }, [dispatch]);

  return (
    <Box>
      {news.map(item => (
        <NewsCard
          key={item.id}
          newsItem={item}
        />
      ))}
    </Box>
  );
};
