export enum ThemeTypeEnum {
  LIGHT = 'light',
  DARK = 'dark'
}
