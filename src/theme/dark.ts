
import { createTheme, type ThemeOptions } from '@mui/material/styles';
import { ThemeTypeEnum } from './enums';

const darkTheme: ThemeOptions = createTheme({
  palette: {
    mode: ThemeTypeEnum.DARK
  }
});

export default createTheme(darkTheme);
