import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import lightTheme from './light';
import darkTheme from './dark';
import { ThemeTypeEnum } from './enums';
import { selectTheme } from 'src/store/slices/app.slice';
import { useStoreSelector } from 'src/store/hooks';

interface IMuiProviderProps {
  children: React.ReactNode;
};

const MaterialProvider: React.FC<IMuiProviderProps> = ({ children }: IMuiProviderProps) => {
  const themeType = useStoreSelector(selectTheme);
  const appTheme = themeType === ThemeTypeEnum.LIGHT ? lightTheme : darkTheme;
  return (
    <ThemeProvider theme={appTheme}>
      {children}
    </ThemeProvider>
  );
};

export default MaterialProvider;
