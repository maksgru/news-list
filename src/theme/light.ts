import { createTheme, type ThemeOptions } from '@mui/material/styles';
import { colors } from '@mui/material';
import { ThemeTypeEnum } from './enums';

const theme: ThemeOptions = createTheme({
  palette: {
    mode: ThemeTypeEnum.LIGHT,
    background: {
      default: colors.blueGrey['50']
    }
  }
});

export default createTheme(theme);
