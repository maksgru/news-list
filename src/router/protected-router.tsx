import React, { type ReactNode } from 'react';
import { Navigate, useLocation } from 'react-router-dom';

export const ProtectedRouter: React.FC<{ children: ReactNode }> = ({ children }) => {
  const location = useLocation();
  const isAuthenticated = true;

  if (!isAuthenticated) {
    return <Navigate to="/login" state={{ from: location }} replace />;
  }
  return <>{children}</>;
};
