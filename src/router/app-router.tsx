import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { routes } from './routes/app-routes';
import { ProtectedRouter } from './protected-router';

export const AppRouter: React.FC = () => {
  return (
    <Routes>
      {routes.map(r => (
        <Route
          key={r.path}
          path={r.path}
          element={(
            <ProtectedRouter>
              {r.element}
            </ProtectedRouter>
          )}
        />
      ))}
    </Routes>
  );
};
