import { type RouteObject } from 'react-router-dom';

export type TAppRoutes = RouteObject & {
  role: string;
};
