import React from 'react';
import { type TAppRoutes } from './app-routes.type';
import { MainPage } from '../../app/pages/main/main.page';
import { NewsPage } from 'src/app/pages/news/news.page';

export const routes: TAppRoutes[] = [
  {
    path: '/',
    element: <MainPage />,
    role: 'user'
  },
  {
    path: '/news/:id',
    element: <NewsPage />,
    role: 'user'
  }
];
