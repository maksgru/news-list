export const storageKeys = {
  ACCESS_TOKEN_KEY: btoa('ACCESS_TOKEN_KEY'),
  REFRESH_TOKEN_KEY: btoa('REFRESH_TOKEN_KEY'),
  THEME_KEY: btoa('THEME_KEY')
};
