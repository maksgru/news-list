import { ThemeTypeEnum } from 'src/theme/enums';
import { storageKeys } from './storage-keys';

export const storage = {
  getAccessToken: () => localStorage.getItem(storageKeys.ACCESS_TOKEN_KEY),
  getRefreshToken: () => localStorage.getItem(storageKeys.REFRESH_TOKEN_KEY),
  setAccessToken: (token: string) => {
    localStorage.setItem(storageKeys.ACCESS_TOKEN_KEY, token);
  },
  setRefreshToken: (token: string) => {
    localStorage.setItem(storageKeys.REFRESH_TOKEN_KEY, token);
  },
  dropStorage: () => {
    localStorage.clear();
  },
  getTheme: (): ThemeTypeEnum => {
    const theme = localStorage.getItem(storageKeys.THEME_KEY);
    if (theme == null) {
      storage.setTheme(ThemeTypeEnum.LIGHT);

      return ThemeTypeEnum.LIGHT;
    }

    return theme as ThemeTypeEnum;
  },
  setTheme: (theme: ThemeTypeEnum) => {
    localStorage.setItem(storageKeys.THEME_KEY, theme);
  }
};
