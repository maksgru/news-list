import { configureStore, type ThunkAction, type Action } from '@reduxjs/toolkit';
import newsSlice from './slices/news.slice';
import appSlice from './slices/app.slice';
import profileSlice from './slices/profile.slice';

export const store = configureStore({
  reducer: {
    app: appSlice,
    profile: profileSlice,
    news: newsSlice
  }
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
