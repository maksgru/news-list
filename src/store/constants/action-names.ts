export const actionNames = {
  PROFILE_SIGN_IN: 'profile/signIn',
  NEWS_GET_ALL: 'news/getAll',
  NEWS_GET_ONE_BY_ID: 'news/getOneById'
};
