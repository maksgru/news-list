import { type ActionStatusEnum } from '../../app/enums/request-status.enum';
import { type INewsItem } from '../../app/interfaces/news-item.interface';

export interface INewsState {
  newsList: INewsItem[];
  selectedNews: INewsItem | null;
  status: ActionStatusEnum;
}
