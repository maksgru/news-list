import { type ActionStatusEnum } from '../../app/enums/request-status.enum';
import { type IProfile } from '../../app/interfaces/profile.interface';

export interface IProfileState {
  user: IProfile | null;
  status: ActionStatusEnum;
}
