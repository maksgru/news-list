import { type ThemeTypeEnum } from 'src/theme/enums';

export interface IAppState {
  theme: ThemeTypeEnum;
}
