import { createAsyncThunk, createSlice, type PayloadAction } from '@reduxjs/toolkit';
import { ActionStatusEnum } from 'src/app/enums/request-status.enum';
import { type INewsState } from '../interfaces/news-state.interface';
import { type INewsItem } from 'src/app/interfaces/news-item.interface';
import { newsService } from 'src/app/services/news';
import { actionNames } from '../constants/action-names';
import { sliceNames } from '../constants/slice-names';
import { useStoreSelector } from '../hooks';

const initialState: INewsState = {
  newsList: [],
  selectedNews: null,
  status: ActionStatusEnum.SUCCESS
};

export const getNews = createAsyncThunk(
  actionNames.NEWS_GET_ALL,
  async () => {
    const response = await newsService.fetchNews();

    return response;
  }
);

export const getNewsItemById = createAsyncThunk(
  actionNames.NEWS_GET_ONE_BY_ID,
  async (id: string) => {
    const response = await newsService.fetchNewsItemById(id);

    return response;
  }
);

export const newsSlice = createSlice({
  name: sliceNames.NEWS,
  initialState,
  reducers: {
    deleteNewsItem: (state, action: PayloadAction<string>) => {
      state.newsList = state.newsList.filter(({ id }) => id !== action.payload);
    },
    setSelectedNews: (state, action: PayloadAction<string>) => {
      state.selectedNews = state.newsList.find(({ id }) => id === action.payload) ?? null;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getNews.pending, (state) => {
        state.status = ActionStatusEnum.LOADING;
      })
      .addCase(getNews.rejected, (state) => {
        state.status = ActionStatusEnum.FAILED;
      })
      .addCase(getNews.fulfilled, (state, action: PayloadAction<INewsItem[]>) => {
        state.status = ActionStatusEnum.SUCCESS;
        state.newsList = action.payload;
      });

    builder
      .addCase(getNewsItemById.fulfilled, (state, action: PayloadAction<INewsItem>) => {
        state.selectedNews = action.payload;
      });
  }
});

export const { deleteNewsItem, setSelectedNews } = newsSlice.actions;

export const selectChosenNews = (): INewsItem => useStoreSelector(s => s.news.selectedNews) as INewsItem;

export default newsSlice.reducer;
