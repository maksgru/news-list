import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import { type RootState } from '..';
import { type IAppState } from '../interfaces/app-state.interface';
import { type ThemeTypeEnum } from 'src/theme/enums';
import { storage } from 'src/utils/storage';
import { sliceNames } from '../constants/slice-names';

const initialState: IAppState = {
  theme: storage.getTheme()
};

export const appSlice = createSlice({
  name: sliceNames.APP,
  initialState,
  reducers: {
    toggleTheme: (state, action: PayloadAction<ThemeTypeEnum>) => {
      state.theme = action.payload;
      storage.setTheme(state.theme);
    }
  }
});
export const selectTheme = (state: RootState): ThemeTypeEnum => state.app.theme;
export const { toggleTheme } = appSlice.actions;

export default appSlice.reducer;
