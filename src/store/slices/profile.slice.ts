import { createAsyncThunk, createSlice, type PayloadAction } from '@reduxjs/toolkit';
import { ActionStatusEnum } from 'src/app/enums/request-status.enum';
import { actionNames } from '../constants/action-names';
import { sliceNames } from '../constants/slice-names';
import { useStoreSelector } from '../hooks';
import { type IProfileState } from '../interfaces/profile-state.interface';
import { storage } from '../../utils/storage';
import { type IProfile } from '../../app/interfaces/profile.interface';

const initialState: IProfileState = {
  user: null,
  status: ActionStatusEnum.SUCCESS
};

export const signIn = createAsyncThunk(
  actionNames.PROFILE_SIGN_IN,
  async () => {
    const response = await Promise.resolve({ id: 'id', name: 'user_name' });

    return response;
  }
);

export const profileSlice = createSlice({
  name: sliceNames.PROFILE,
  initialState,
  reducers: {
    signOut: (state) => {
      storage.dropStorage();
      state.user = null;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(signIn.pending, (state) => {
        state.status = ActionStatusEnum.LOADING;
      })
      .addCase(signIn.rejected, (state) => {
        state.status = ActionStatusEnum.FAILED;
      })
      .addCase(signIn.fulfilled, (state, action: PayloadAction<IProfile>) => {
        state.status = ActionStatusEnum.SUCCESS;
        state.user = action.payload;
      });
  }
});

export const { signOut } = profileSlice.actions;

export const selectProfile = (): IProfile => useStoreSelector(s => s.profile.user) as IProfile;

export default profileSlice.reducer;
