# React/Redux-toolkit base app

## How to start
```sh
yarn install
```

```sh
yarn start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


Application is using [Material UI components library](https://mui.com/material-ui/react-autocomplete/).
